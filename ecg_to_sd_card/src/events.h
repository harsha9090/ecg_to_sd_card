// Copyright 2017 Firia, Inc.
/* events.h
Asynchronous event scheduler.

A lightweight scheduler with fast constant-time schedule/unschedule performance.

Event loop features multiple levels of timer granularity for runtime efficiency, coupled
with the flexibility of arbitrary time-scheduling.

*/
#ifndef __events_h_
#define __events_h_

#include <stdint.h>

// Event object
typedef struct _tag_Event {
  void (*run)(struct _tag_Event *ev);
  void *cookie;
  uint32_t ticks_rem;
  uint32_t ticks_reload; // (== 0 for a one-shot event)
  struct _tag_Event *prev;
  struct _tag_Event *next;
} Event;

// Schedule event on an explicit timer list, firing first in 'initial_ticks' counts,
// firing again in 'recurring_ticks' counts *IFF* non-zero.
void schedule_event_ticks(Event *ev, void (*func)(Event *), uint32_t initial_ticks, uint32_t recurring_ticks, Event *timer_list);

// Schedule event to fire initially in 'initial_ms' milliseconds, optionally firing
// again every 'recurring_ms' if that parameter is non-zero.
// The event will be put on a timer_list with sufficient resolution for given value.
// Note: Round your 'xx_ms' args to the nearest acceptable power of 10 for greatest runtime efficiency.
void schedule_event_ms(Event *ev, void (*func)(Event *), uint32_t initial_ms, uint32_t recurring_ms);

// Unschedule event. Event object may be immediately reused on any timer list.
void unschedule_event(Event *ev);

// Application entry points
void event_loop(void);
void isr_tick1ms(void);

// Timer lists maintained in event_loop, extern'd here for explicit schedule_event() calls.
extern Event * const immediate_list;
extern Event * const timer1ms_list;
extern Event * const timer10ms_list;
extern Event * const timer100ms_list;

#endif   // __events_h_
