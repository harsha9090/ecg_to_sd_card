/*
 * buffer_routines.h
 *
 *  Created on: Jul 8, 2020
 *      Author: Harsha Ganegoda
 */

#ifndef BUFFER_ROUTINES_H_
#define BUFFER_ROUTINES_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>



// 12 x 2048 big buffer
#define BUF_SIZE 		(2048*5) //10240 //(2048*12) //24576 = (2048*12)
// ECG byte count per sample
#define ECG_BYTE_COUNT  8

// Number of bytes per sub page
#define SUB_BUFFER_COUNT 2048

#define SUB_LOCS 3

uint8_t bigBuffer[BUF_SIZE];

typedef struct {
	uint32_t currentAvailablePtr;
	uint32_t nextAvailablePtr;
	uint8_t pos;
	uint32_t *loc;
	bool isFull;
	bool readyToSend;

} indexes;

typedef struct{
	uint32_t subBufLocs[SUB_LOCS];
	uint32_t len;

} locationsToDescs;

indexes  get_indexes(void);
locationsToDescs get_subbuffer_memory_locs(void);

void fill_buffer(uint8_t smallBuffer[]);
uint8_t * get_big_buffer(void);



#endif /* BUFFER_ROUTINES_H_ */
