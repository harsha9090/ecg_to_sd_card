/*
 * gpio_interrupt.c
 *
 *  Created on: Jul 8, 2020
 *      Author: Harsha Ganegoda
 */

#include "gpio_interrupt.h"
#include "hw_config.h"
#include "microsdconfig.h"

#include "em_emu.h"
#include "em_gpio.h"
#include "em_int.h"
#include "em_cmu.h"

void init_gpio(void)
{
	// Enable GPIO clock
	CMU_ClockEnable(cmuClock_GPIO, true);
	// Configure TEST_GPIO_1 as an output
	GPIO_PinModeSet(TEST_GPIO_1_PORT, TEST_GPIO_1_PIN, gpioModePushPull, 0);
	// Configure ECG_DRDY as an input
	GPIO_PinModeSet(ECG_DRDY_PORT, ECG_DRDY_PIN, gpioModeInputPullFilter, 1);
	GPIO_IntConfig(ECG_DRDY_PORT, ECG_DRDY_PIN, true, false, true);

	//Reset ADS1293
	GPIO_PinModeSet(ECG_RST_PORT, ECG_RST_PIN, gpioModePushPull, 1);

	// Configure RX pin as an input
	GPIO_PinModeSet(MICROSD_MISOPORT, MICROSD_MISOPIN, gpioModeInput, 0);
	// Configure TX pin as an output
	GPIO_PinModeSet(MICROSD_MOSIPORT, MICROSD_MOSIPIN, gpioModePushPull, 0);
	// Configure CLK pin as an output low (CPOL = 0)
	GPIO_PinModeSet(MICROSD_CLKPORT, MICROSD_CLKPIN, gpioModePushPull, 0);
	// Configure CS pin as an output and drive inactive high
	GPIO_PinModeSet(MICROSD_GPIOPORT, MICROSD_CSPIN, gpioModePushPull, 1);

	//Speed measure pin setup
	GPIO_PinModeSet(TIME_MEASURE_GPIOPORT, TIME_MEASURE_PIN, gpioModePushPull, 0);

	uint32_t rstCnt = 0;
	while(rstCnt < 2000)
	{
		GPIO_PinOutClear(ECG_RST_PORT, ECG_RST_PIN);
		rstCnt++;
	}
	GPIO_PinOutSet(ECG_RST_PORT, ECG_RST_PIN);

	// Enable IRQ for odd numbered GPIO pins
	NVIC_ClearPendingIRQ(GPIO_EVEN_IRQn);
	NVIC_EnableIRQ(GPIO_EVEN_IRQn);

}
