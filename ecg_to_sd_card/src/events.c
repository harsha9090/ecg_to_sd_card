// Copyright 2017 Firia, Inc.
#include "events.h"
#include "sys_defines.h"

// Timer lists are circular double-linked, to provide fast insert at head or tail as well as delete anywhere.
// Event objects hold list head (.next) and tail (.prev) pointers.
static Event immediate_ev = { .prev=&immediate_ev, .next=&immediate_ev };
static Event timer1ms_ev = { .prev=&timer1ms_ev, .next=&timer1ms_ev };
static Event timer10ms_ev = { .prev = &timer10ms_ev,.next = &timer10ms_ev };
static Event timer100ms_ev = { .prev = &timer100ms_ev,.next = &timer100ms_ev };
Event * const immediate_list = &immediate_ev;
Event * const timer1ms_list = &timer1ms_ev;
Event * const timer10ms_list = &timer10ms_ev;
Event * const timer100ms_list = &timer100ms_ev;

// Flags set in ISR, cleared in event loop
static uint8_t tick1ms, tick10ms, tick100ms;

// On-deck and terminal events in current run_events() cycle
static Event *cur_next = 0;
static Event *cur_end = 0;

void schedule_event_ticks(Event *ev, void(*func)(Event *), uint32_t initial_ticks, uint32_t recurring_ticks, Event *timer_list)
{
  FIRIA_irqState irqState;
  irqState = FIRIA_EnterCritical();

  // If this event object has been used, make sure it's unscheduled.
  // Note: Added check of ev->run to avoid trying to unschedule garbage uninitialized Event objects allocated on the stack vs statically.
  //       There is still some risk with the following when the Event is not zero'd prior to scheduling.
  if (ev->prev && ev->run == func)
  {
    unschedule_event(ev);
  }

  // Initialize event object
  ev->run = func;
  ev->ticks_rem = initial_ticks;
  ev->ticks_reload = recurring_ticks; // (possibly 0)
  ev->next = timer_list;  // Terminal item points to list. Empty list has prev=next=list.

  // If first append this cycle, mark this as new cur_end
  if (cur_end == timer_list)
  {
    cur_end = ev;

    // If at end of cycle, update cur_next to reference new cur_end
    if (cur_next == timer_list)
    {
      cur_next = cur_end;
    }
  }

  // Append event to end of selected list
  ev->prev = timer_list->prev;
  ev->prev->next = ev;
  timer_list->prev = ev;
  FIRIA_ExitCritical(irqState);
}

void unschedule_event(Event *ev)
{
  FIRIA_irqState irqState;
  irqState = FIRIA_EnterCritical();
  Event *next = ev->next;
  ev->prev->next = next;
  next->prev = ev->prev;
  ev->prev = 0;
  if (ev == cur_next)
  {
    cur_next = next;
  }
  if (ev == cur_end)
  {
    cur_end = next;
  }
  FIRIA_ExitCritical(irqState);
}

void schedule_event_ms(Event *ev, void (*func)(Event *), uint32_t initial_ms, uint32_t recurring_ms)
{
  Event *timer_list;
  // Determine appropriate timer list based on resolution
  if (!initial_ms)
  {
    timer_list = immediate_list;
    initial_ms = 1;
    recurring_ms = 0; // We don't support immediate+recurring events
  }
  else if (!(initial_ms % 100))
  {
    timer_list = timer100ms_list;
    initial_ms /= 100;
    recurring_ms /= 100;
  }
  else if (!(initial_ms % 10))
  {
    timer_list = timer10ms_list;
    initial_ms /= 10;
    recurring_ms /= 10;
  }
  else
  {
    timer_list = timer1ms_list;
  }

  schedule_event_ticks(ev, func, initial_ms, recurring_ms, timer_list);
}

static void run_events(Event *list)
{
  FIRIA_irqState irqState = FIRIA_EnterCritical();
  Event *ev = list->next;
  cur_end = list;
  while (ev != cur_end)
  {
    cur_next = ev->next;
    --ev->ticks_rem;
    if (!ev->ticks_rem)
    {

      // If this was a one-shot event
      if (!ev->ticks_reload)
      {
        // Remove from list
        ev->prev->next = cur_next;
        cur_next->prev = ev->prev;
        ev->prev = 0;
      }
      else
      {
        // Simply reschedule it
        ev->ticks_rem = ev->ticks_reload;
      }

      FIRIA_ExitCritical(irqState);
      // Run event
      ev->run(ev);
      irqState = FIRIA_EnterCritical();
    }
    ev = cur_next;
  }

  FIRIA_ExitCritical(irqState);
}

void event_loop(void)
{
  while (1)
  {
    // Invoke 'immediate' events here
    run_events(immediate_list);

    if (tick1ms)
    {
      tick1ms = 0;
      run_events(timer1ms_list);
    }
    if (tick10ms)
    {
      tick10ms = 0;
      run_events(timer10ms_list);
    }
    if (tick100ms)
    {
      tick100ms = 0;
      run_events(timer100ms_list);
    }
  }
}

void isr_tick1ms(void)
{
  static uint8_t countdown_10ms = 10, countdown_100ms = 10;
  tick1ms = 1;

  if (!--countdown_10ms)
  {
    tick10ms = 1;
    countdown_10ms = 10;

    if (!--countdown_100ms)
    {
      tick100ms = 1;
      countdown_100ms = 10;
    }
  }
}
