/*
 * buffer_routines.c
 *
 *  Created on: Jul 8, 2020
 *      Author: Harsha Ganegoda
 */

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "buffer_routines.h"
#include "hw_config.h"

uint32_t byteItteration = 0;
uint32_t byteCount = 0;
static indexes ecgIdx;
locationsToDescs memLocs;

indexes get_indexes(void)
{
	return ecgIdx;
}

locationsToDescs get_subbuffer_memory_locs(void)
{
	return memLocs;
}

uint8_t * get_big_buffer(void)
{
	return bigBuffer;
}

void fill_buffer(uint8_t smallBuffer[])
{
	ecgIdx.currentAvailablePtr = ecgIdx.nextAvailablePtr;
	memcpy(bigBuffer + ecgIdx.currentAvailablePtr, smallBuffer, ECG_BYTE_COUNT);
	byteItteration++;
	byteCount = byteItteration * ECG_BYTE_COUNT;
	ecgIdx.isFull = false;
	ecgIdx.readyToSend = false;
	if(byteCount == SUB_BUFFER_COUNT)
	{
		ecgIdx.isFull = true;
		byteCount = 0;
		byteItteration = 0;
		memLocs.subBufLocs[ecgIdx.pos] = &bigBuffer[ecgIdx.currentAvailablePtr - (SUB_BUFFER_COUNT - ECG_BYTE_COUNT)];
		ecgIdx.pos++;
		if(ecgIdx.pos == SUB_LOCS)
		{
			byteItteration = 0;
			byteCount = 0;
			ecgIdx.readyToSend = true;
			ecgIdx.pos = 0;
		}
	}

	ecgIdx.nextAvailablePtr = (ecgIdx.nextAvailablePtr + ECG_BYTE_COUNT) % BUF_SIZE;
}



