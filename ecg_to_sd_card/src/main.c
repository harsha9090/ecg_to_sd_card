/*
 * This code is checked on SiLabs BRD4180A Rev A01 radio board
 * which has EFR32MG21 Series 2 chip
 *
 * Buffering ECG data into micro sd card
 *
 */

#include "em_device.h"
#include "em_cmu.h"
#include "em_emu.h"
#include "em_timer.h"
#include "em_chip.h"
#include "stdio.h"
#include "fatfs_file.h"
#include "hw_config.h"
#include "events.h"
#include "ecg_ads1293.h"
#include "gpio_interrupt.h"
#include "buffer_routines.h"

static uint8_t wakeUpSource = 0;
uint8_t *ecgBuffer;
uint8_t ecgBytes[8];
static indexes buffer_idxs;
locationsToDescs mem_idxs;
uint32_t first_write_loc = 0;

void GPIO_EVEN_IRQHandler(void)
{
	// Get and clear all pending GPIO interrupts
	uint32_t interruptMask = GPIO_IntGet();
	GPIO_IntClear(interruptMask);

	if (interruptMask & (1 << ECG_DRDY_PIN))
	{
		wakeUpSource = 1;
		GPIO_PinOutSet(TIME_MEASURE_GPIOPORT, TIME_MEASURE_PIN);
		//read ecg data
		ecgBuffer = stream_ads1293_data(ecgBytes);
		// Buffer ecg data into BIG BUFFER
		fill_buffer(ecgBuffer);
		GPIO_PinOutClear(TIME_MEASURE_GPIOPORT, TIME_MEASURE_PIN);
	}
}

int main(void)
{
	// Chip errata
	CHIP_Init();
	// Initialization of GPIO and IRQ
	init_gpio();
	// Initialization of micro sd card
	init_micro_sd();
	file_mount();
	// Initialization of ADS1293
	init_ads1293();

	while(1)
	{
		buffer_idxs = get_indexes();
		if(buffer_idxs.readyToSend)
		{
			//Buffer is FULL
			file_open_for_write();
			mem_idxs = get_subbuffer_memory_locs();
			for(uint8_t i = 0; i < SUB_LOCS; i++)
			{
				file_lseek(first_write_loc);
				uint8_t *b = (uint8_t *)(mem_idxs.subBufLocs[i]);
				file_write(b, SUB_BUFFER_COUNT);
				first_write_loc += SUB_BUFFER_COUNT;
			}
			file_close();
		}
	}
}
