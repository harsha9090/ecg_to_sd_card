
#ifndef __HW_CONFIG_H
#define __HW_CONFIG_H

#include "em_device.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_usart.h"
#include "em_ldma.h"

// TODO: When integrating all the sensors to the single board, following SD card peripheral should be configured
#define SD_CARD_WAKEUP_TIME 100 // ms
#define ECG_HOLD_RESET_TIME 1 // ms

#define SD_CARD_RX_DMA_CHANNEL 0
#define SD_CARD_TX_DMA_CHANNEL 1

#define SD_CARD_DMA_RX_PERIPH_SIGNAL (LDMAXBAR_CH_REQSEL_SIGSEL_USART0RXDATAV | LDMAXBAR_CH_REQSEL_SOURCESEL_USART0)
#define SD_CARD_DMA_TX_PERIPH_SIGNAL (LDMAXBAR_CH_REQSEL_SIGSEL_USART0TXBL | LDMAXBAR_CH_REQSEL_SOURCESEL_USART0)

#define SD_CARD_USART    USART0
#define SD_CARD_CMU_CLK  cmuClock_USART0

#define SD_CARD_MISO_PORT      gpioPortA
#define SD_CARD_MISO_PIN       7

#define SD_CARD_MOSI_PORT      gpioPortA
#define SD_CARD_MOSI_PIN       8

#define SD_CARD_CLK_PORT       gpioPortA
#define SD_CARD_CLK_PIN        9

#define SD_CARD_CS_PORT        gpioPortA
#define SD_CARD_CS_PIN         10

#define SD_CARD_PWR_EN_PORT    gpioPortA
#define SD_CARD_PWR_EN_PIN     11

// ECG DMA
#define ECG_RX_DMA_CHANNEL 2
#define ECG_TX_DMA_CHANNEL 3
#define ECG_DMA_RX_PERIPH_SIGNAL (LDMAXBAR_CH_REQSEL_SIGSEL_USART0RXDATAV | LDMAXBAR_CH_REQSEL_SOURCESEL_USART0)
#define ECG_DMA_TX_PERIPH_SIGNAL (LDMAXBAR_CH_REQSEL_SIGSEL_USART0TXBL | LDMAXBAR_CH_REQSEL_SOURCESEL_USART0)

// ECG PORTS and PINS
#define ECG_USART    USART0
#define ECG_CMU_CLK  cmuClock_USART0

#define ECG_MISO_PORT      gpioPortC
#define ECG_MISO_PIN       4

#define ECG_MOSI_PORT      gpioPortC
#define ECG_MOSI_PIN       5

#define ECG_CLK_PORT       gpioPortA
#define ECG_CLK_PIN        6

#define ECG_CS_PORT        gpioPortA
#define ECG_CS_PIN         0

#define ECG_DRDY_PORT       gpioPortD
#define ECG_DRDY_PIN        2

#define ECG_RST_PORT       gpioPortA
#define ECG_RST_PIN        3

#define TEST_GPIO_1_PORT       gpioPortB
#define TEST_GPIO_1_PIN        1

// MICRO_SD PORTS and PINS
#define MICROSD_HI_SPI_FREQ     20000000 //8000000

#define MICROSD_LO_SPI_FREQ     100000
#define MICROSD_USART           USART2 			//USART0
#define MICROSD_CMUCLOCK        cmuClock_USART2 //cmuClock_USART0

#define MICROSD_MISOPORT		gpioPortC 		//gpioPortC = BRD4183A
#define MICROSD_MISOPIN         0 				//4 = BRD4183A
#define MICROSD_MOSIPORT		gpioPortC 		//gpioPortC = BRD4183A
#define MICROSD_MOSIPIN         1 				//5 = BRD4183A
#define MICROSD_CLKPORT			gpioPortD 		//gpioPortA //gpioPortA = BRD4183A
#define MICROSD_CLKPIN          4 				//4 = BRD4183A

#define MICROSD_GPIOPORT        gpioPortC 		//gpioPortA = BRD4183A
#define MICROSD_CSPIN           2 				//0 = BRD4183A

#define TIME_MEASURE_GPIOPORT  	gpioPortB 		//gpioPortB = BRD4183A
#define TIME_MEASURE_PIN		0 				//1 = BRD4183A

#endif // __HW_CONFIG_H

