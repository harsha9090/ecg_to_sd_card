#ifndef __SPI_DRIVER_H
#define __SPI_DRIVER_H

#include <stdint.h>

typedef void (*spi_xfer_cb)();


void sd_card_spi_init(void);
void sd_card_spi_deinit(void);

void sd_card_spi_start_sync_tx(uint8_t *tx_buf, uint8_t *rx_buf, uint32_t len_bytes, spi_xfer_cb cb);


void ecg_spi_init(void);
void ecg_spi_deinit(void);

void ecg_spi_start_sync_tx(uint8_t *tx_buf, uint8_t *rx_buf, uint32_t len_bytes, spi_xfer_cb cb);

#endif // __SPI_DRIVER_H
