/*
 * ecg_ads1293.h
 *
 *  Created on: Jun 30, 2020
 *      Author: Harsha Ganegoda
 */

#ifndef ECG_ADS1293_H_
#define ECG_ADS1293_H_

#include "stdint.h"
#include "spi_driver.h"

void init_ads1293(void);
void write_ads1293_register(uint8_t writeRegister, uint8_t value);
uint8_t read_ads1293_register(uint8_t readRegister);
uint8_t *stream_ads1293_data(uint8_t ecgBytes[]);

#endif /* ECG_ADS1293_H_ */
