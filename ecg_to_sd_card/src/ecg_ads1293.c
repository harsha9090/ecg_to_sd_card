/*
 * ecg_ads1293.c
 *
 *  Created on: Jun 30, 2020
 *      Author: Harsha Ganegoda
 */

#include "ecg_ads1293.h"
#include "hw_config.h"
#include "TI_ADS1293.h"
#include "TI_ADS1293_register_settings.h"

static uint8_t tx_buf[12];
static uint8_t rx_buf[12];

void config_ads1293_registers(void);
void spi_tx_complete(void);
void spi_rx_complete(void);

void init_ads1293(void)
{
	// Initialization of SPI
	ecg_spi_init();
	// ADS1293 configuration
	config_ads1293_registers();
}

void config_ads1293_registers(void)
{

    // Same Medicomp configurations
    // Operation Mode Registers
	write_ads1293_register(TI_ADS1293_CONFIG_REG,         0x00);
    // Input Channel Selection Registers
	write_ads1293_register(TI_ADS1293_FLEX_CH1_CN_REG,    0x29);  // [00 101 001] CH1+ LL(IN5), CH1- RA(IN1)
	write_ads1293_register(TI_ADS1293_FLEX_CH2_CN_REG,    0x2B);  // [00 101 011] CH1+ LL(IN5), CH1- LA(IN3)
	write_ads1293_register(TI_ADS1293_FLEX_CH3_CN_REG,    0x00);
	write_ads1293_register(TI_ADS1293_FLEX_PACE_CN_REG,   0x00);
	write_ads1293_register(TI_ADS1293_FLEX_VBAT_CN_REG,   0x00);
    // Lead-off Detect Control Registers
	write_ads1293_register(TI_ADS1293_LOD_CN_REG,         0x08);  // LOD Shutdown
	write_ads1293_register(TI_ADS1293_LOD_EN_REG,         0x00);
	write_ads1293_register(TI_ADS1293_LOD_CURRENT_REG,    0x00);
	write_ads1293_register(TI_ADS1293_LOD_AC_CN_REG,      0x00);
    // Common-Mode Detection and Right-Leg Drive Feedback Control Registers
	write_ads1293_register(TI_ADS1293_CMDET_EN_REG,       0x15);  // [-- 010101] RA(IN1), LA(IN3) & LL(IN5)
	write_ads1293_register(TI_ADS1293_CMDET_CN_REG,       0x00);
	write_ads1293_register(TI_ADS1293_RLD_CN_REG,         0x02);  // [- 0 00 0 010] RLD out IN2
    // Wilson Control Registers
	write_ads1293_register(TI_ADS1293_WILSON_EN1_REG,     0x00);
	write_ads1293_register(TI_ADS1293_WILSON_EN2_REG,     0x00);
	write_ads1293_register(TI_ADS1293_WILSON_EN3_REG,     0x00);
	write_ads1293_register(TI_ADS1293_WILSON_CN_REG,      0x00);
    // Reference Registers
	write_ads1293_register(TI_ADS1293_REF_CN_REG,         0x00);
    // OSC Control Registers
	write_ads1293_register(TI_ADS1293_OSC_CN_REG,         0x04);  // [----- 1 0 0] start clock
    // AFE Control Registers
	write_ads1293_register(TI_ADS1293_AFE_RES_REG,        0x00);
	write_ads1293_register(TI_ADS1293_AFE_SHDN_CN_REG,    0x24);  // [-- 100 100] shutdown INA3 & SDM3
	write_ads1293_register(TI_ADS1293_AFE_FAULT_CN_REG,   0x04);	// enable fault detection for chan 1 & 2, disable chan 3
	write_ads1293_register(TI_ADS1293_AFE_DITHER_EN_REG,  0x00);
	write_ads1293_register(TI_ADS1293_AFE_PACE_CN_REG,    0x05);  // shutdown analog pace and route RLDIN to RLDREF
    // Digital Registers
	write_ads1293_register(TI_ADS1293_DIGO_STRENGTH,      0x00);  // 00 = low drive mode
	write_ads1293_register(TI_ADS1293_R2_RATE_REG,        0x02);  // R2 = 5
	write_ads1293_register(TI_ADS1293_R3_RATE1_REG,       0x04);  // R3 = 8
	write_ads1293_register(TI_ADS1293_R3_RATE2_REG,       0x04);  // R3 = 8
	write_ads1293_register(TI_ADS1293_R3_RATE3_REG,       0x00);
	write_ads1293_register(TI_ADS1293_P_DRATE_REG,        0x00);  // R1 = 4
	write_ads1293_register(TI_ADS1293_DIS_EFILTER_REG,    0x00);
	write_ads1293_register(TI_ADS1293_DRDYB_SRC_REG,      0x08);  // [-- 001 000] DRDY driven by CH1
	write_ads1293_register(TI_ADS1293_SYNCOUTB_SRC_REG,   0x00);
	write_ads1293_register(TI_ADS1293_MASK_DRDYB_REG,     0x00);
	write_ads1293_register(TI_ADS1293_MASK_ERR_REG,       0xCC);	// enable CH1, CH2, RLDRAIL, & CMOR; all others masked on ALARMB pin
	write_ads1293_register(TI_ADS1293_ALARM_FILTER_REG,   0x40);	// 0 counts for LOD; 4 counts for all other alarms
	write_ads1293_register(TI_ADS1293_CH_CNFG_REG,        0x31);   // [- 0110001] enable loop read for DATA_STATUS and CH1/CH2 ECG (7 bytes)

	write_ads1293_register(TI_ADS1293_CONFIG_REG, (0x00 | 0x01)); //Starts data conversion

/*
	write_ads1293_register(TI_ADS1293_CONFIG_REG, TI_ADS1293_CONFIG_REG_VALUE); //Turn on ADS1293
	write_ads1293_register(TI_ADS1293_FLEX_CH1_CN_REG, TI_ADS1293_FLEX_CH1_CN_REG_VALUE);  //Connect channel 1�s INP to IN2 and INN to IN1
	write_ads1293_register(TI_ADS1293_FLEX_CH2_CN_REG, TI_ADS1293_FLEX_CH2_CN_REG_VALUE);  //Connect channel 2�s INP to IN3 and INN to IN1
	write_ads1293_register(TI_ADS1293_CMDET_EN_REG, TI_ADS1293_CMDET_EN_REG_VALUE);  //Enable the common-mode detector on input pins IN1, IN2 and IN3
	write_ads1293_register(TI_ADS1293_RLD_CN_REG, TI_ADS1293_RLD_CN_REG_VALUE);  //Connect the output of the RLD amplifier internally to pin IN4
	write_ads1293_register(TI_ADS1293_OSC_CN_REG, TI_ADS1293_OSC_CN_REG_VALUE);  //Use external crystal and feed the internal oscillator's output to the digital
	write_ads1293_register(TI_ADS1293_AFE_SHDN_CN_REG, TI_ADS1293_AFE_SHDN_CN_REG_VALUE);  //Shuts down unused channel 3�s signal path
	write_ads1293_register(TI_ADS1293_R2_RATE_REG, TI_ADS1293_R2_RATE_REG_VALUE);  //Configures the R2 decimation rate as 5 for all channels
	write_ads1293_register(TI_ADS1293_R3_RATE1_REG, TI_ADS1293_R3_RATE1_REG_VALUE);  //Configures the R3 decimation rate as 6 for channel 1
	write_ads1293_register(TI_ADS1293_R3_RATE2_REG, TI_ADS1293_R3_RATE2_REG_VALUE);  //Configures the R3 decimation rate as 6 for channel 2
	write_ads1293_register(TI_ADS1293_DRDYB_SRC_REG, TI_ADS1293_DRDYB_SRC_REG_VALUE);  //Configures the DRDYB source to channel 1 ECG (or fastest channel)
	write_ads1293_register(TI_ADS1293_CH_CNFG_REG, TI_ADS1293_CH_CNFG_REG_VALUE);  // Enables channel 1 ECG and channel 2 ECG for loop read-back mode
	write_ads1293_register(TI_ADS1293_CONFIG_REG, (0x00 | 0x01)); //Starts data conversion
*/
}

void write_ads1293_register(uint8_t writeRegister, uint8_t value)
{
	tx_buf[0] = writeRegister & ADS1293_WRITE_BIT;
	tx_buf[1] = value;
	ecg_spi_start_sync_tx(tx_buf, rx_buf, 2, spi_tx_complete);
}


uint8_t read_ads1293_register(uint8_t readRegister)
{
	tx_buf[0] = readRegister | ADS1293_READ_BIT;
	ecg_spi_start_sync_tx(tx_buf, rx_buf, 2, spi_rx_complete);
	return rx_buf;
}

uint8_t *stream_ads1293_data(uint8_t ecgBytes[])
{
	tx_buf[0] = TI_ADS1293_DATA_LOOP_REG | ADS1293_READ_BIT;
	ecg_spi_start_sync_tx(tx_buf, rx_buf, ECG_BYTE_COUNT, spi_rx_complete);
	uint8_t i;
	for(i = 0; i < 6; i++)
	{
		ecgBytes[i] = rx_buf[i + 2];
	}

	return ecgBytes;
}

void spi_tx_complete(void)
{
}

void spi_rx_complete(void)
{
}

