/*
 * fatfs_file.c
 *
 *  Created on: Aug 25, 2020
 *      Author: Harsha Ganegoda
 */

#include "ff.h"
#include "microsd.h"
#include "diskio.h"
#include "fatfs_file.h"

//Filename to open/write/read from SD-card
#define ECG_FILENAME    "ecg.txt"


#define BUFFERSIZE      (2048*12)

FIL fsrc;				/* File objects */
FATFS Fatfs;				/* File system specific */
FRESULT res;				/* FatFs function common result code */
UINT br, bw;				/* File read/write count */
DSTATUS resCard;			/* SDcard status */

int16_t i;
int16_t filecounter;

/* Counts 1ms timeTicks */
volatile uint32_t msTicks;

/***************************************************************************//**
 * @brief
 *   This function is required by the FAT file system in order to provide
 *   timestamps for created files. Since this example does not include a
 *   reliable clock we hardcode a value here.
 *
 *   Refer to drivers/fatfs/doc/en/fattime.html for the format of this DWORD.
 * @return
 *    A DWORD containing the current time and date as a packed datastructure.
 ******************************************************************************/
DWORD get_fattime(void)
{
	return (28 << 25) | (2 << 21) | (1 << 16);
}


 //@brief SysTick_Handler
 //Interrupt Service Routine for system tick counter.
void SysTick_Handler(void)
{
	//Increment counter necessary in Delay()
	msTicks++;
}


 //@brief Delays number of msTick Systicks (typically 1 ms)
 //@param dlyTicks Number of ticks to delay
void Delay(uint32_t dlyTicks)
{
	uint32_t curTicks;
	curTicks = msTicks;
	while ((msTicks - curTicks) < dlyTicks) ;
}

void init_micro_sd(void)
{
	while(1)
	{
		MICROSD_Init();                     //Initialize MicroSD driver
		resCard = disk_initialize(0);       //Check micro-SD card status
		switch(resCard)
		{
		case STA_NOINIT:                    // Drive not initialized
			break;
		case STA_NODISK:                    // No medium in the drive
			break;
		case STA_PROTECT:                   // Write protected
			break;
		default:
			break;
		}

		if (!resCard) break;                // Drive initialized.
	}
}

void file_mount(void)
{
	if (f_mount(0, &Fatfs) != FR_OK)
	{
		// Error.No micro-SD with FAT32 is present
		while(1);
	}
}

void file_open_for_write()
{
	res = f_open(&fsrc, ECG_FILENAME,  FA_WRITE);
	if (res != FR_OK)
	{
		//  If file does not exist create it
		res = f_open(&fsrc, ECG_FILENAME, FA_CREATE_ALWAYS | FA_WRITE );
		if (res != FR_OK)
		{
			// Error. Cannot create the file
			while(1);
		}
	}
}

//Test Function
void file_open_for_write_with_file_name(char file_name)
{
	// Open  the file for write
	res = f_open(&fsrc, file_name,  FA_WRITE);
	if (res != FR_OK)
	{
		//  If file does not exist create it
		res = f_open(&fsrc, file_name, FA_CREATE_ALWAYS | FA_WRITE );
		if (res != FR_OK)
		{
			// Error. Cannot create the file
			while(1);
		}
	}
}

void file_lseek(uint32_t start_location)
{
	//Set the file write pointer to first location
	res = f_lseek(&fsrc, start_location);
	if (res != FR_OK)
	{
		// Error. Cannot set the file write pointer
		while(1);
	}
}

void file_write(uint8_t buffer_to_write[], uint32_t num_bytes)
{
	res = f_write(&fsrc, buffer_to_write, num_bytes, &bw);
	if ((res != FR_OK) || (num_bytes != bw))
	{
		// Error. Cannot write the file
		while(1);
	}
}

void file_close(void)
{
	f_close(&fsrc);
	if (res != FR_OK)
	{
		// Error. Cannot close the file
		while(1);
	}
}


