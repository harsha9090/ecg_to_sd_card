#include <stdbool.h>

#include "hw_config.h"
#include "spi_driver.h"
#include "em_core.h"


#define NUM_DESCRIPTORS 10

LDMA_Descriptor_t sd_card_tx_descs[NUM_DESCRIPTORS];
LDMA_Descriptor_t sd_card_rx_descs[NUM_DESCRIPTORS];
LDMA_Descriptor_t ecg_tx_descs[1];
LDMA_Descriptor_t ecg_rx_descs[1];  //LDMA_Descriptor_t desc;

typedef struct {
  uint8_t channel;
  uint32_t chnl_mask;
  LDMA_TransferCfg_t cfg;
  LDMA_Descriptor_t * descs;
} spi_dma_cfg;

typedef struct {
  spi_xfer_cb xfer_compl_cb;
  CMU_Clock_TypeDef cmu_clk;
  USART_TypeDef *usart;
  spi_dma_cfg *rx_dma;
  spi_dma_cfg *tx_dma;
  bool xfer_compl;
} spi_hdle;

static bool usarts_initialized = false;

static spi_dma_cfg ecg_tx_cfg = {
  .channel = ECG_TX_DMA_CHANNEL,
  .chnl_mask = 1 << ECG_TX_DMA_CHANNEL,
  .cfg = (LDMA_TransferCfg_t)LDMA_TRANSFER_CFG_PERIPHERAL(ECG_DMA_TX_PERIPH_SIGNAL),
  .descs = ecg_tx_descs
};

static spi_dma_cfg ecg_rx_cfg = {
  .channel = ECG_RX_DMA_CHANNEL,
  .chnl_mask = 1 << ECG_RX_DMA_CHANNEL,
  .cfg = (LDMA_TransferCfg_t)LDMA_TRANSFER_CFG_PERIPHERAL(ECG_DMA_RX_PERIPH_SIGNAL),
  .descs = ecg_rx_descs
};

static spi_dma_cfg sd_card_tx_cfg = {
  .channel = SD_CARD_TX_DMA_CHANNEL,
  .chnl_mask = 1 << SD_CARD_TX_DMA_CHANNEL,
  .cfg = (LDMA_TransferCfg_t)LDMA_TRANSFER_CFG_PERIPHERAL(SD_CARD_DMA_TX_PERIPH_SIGNAL),
  .descs = sd_card_tx_descs
};

static spi_dma_cfg sd_card_rx_cfg = {
  .channel = SD_CARD_RX_DMA_CHANNEL,
  .chnl_mask = 1 << SD_CARD_RX_DMA_CHANNEL,
  .cfg = (LDMA_TransferCfg_t)LDMA_TRANSFER_CFG_PERIPHERAL(SD_CARD_DMA_RX_PERIPH_SIGNAL),
  .descs = sd_card_rx_descs
};

static spi_hdle ecg = {
  .cmu_clk = ECG_CMU_CLK,
  .usart = ECG_USART,
  .rx_dma = &ecg_rx_cfg,
  .tx_dma = &ecg_tx_cfg,
  .xfer_compl = false
};

static spi_hdle sd_card = {
  .cmu_clk = SD_CARD_CMU_CLK,
  .usart = SD_CARD_USART,
  .rx_dma = &sd_card_rx_cfg,
  .tx_dma = &sd_card_tx_cfg,
  .xfer_compl = false
};


void ecg_spi_init_usart(void)
{
  // Configure GPIO mode for Master
  GPIO_PinModeSet(ECG_CLK_PORT, ECG_CLK_PIN, gpioModePushPull, 0);   // CLK is push pull
  GPIO_PinModeSet(ECG_CS_PORT, ECG_CS_PIN, gpioModePushPull, 1);     // CS is push pull
  GPIO_PinModeSet(ECG_MOSI_PORT, ECG_MOSI_PIN, gpioModePushPull, 1); // TX (MOSI) is push pull
  GPIO_PinModeSet(ECG_MISO_PORT, ECG_MISO_PIN, gpioModeInput, 1);    // RX (MISO) is input

  // Start with default config, then modify as necessary
  USART_InitSync_TypeDef config = USART_INITSYNC_DEFAULT;
  config.master       = true;            // master mode
  config.baudrate     = 10000000;        // CLK freq is 20 MHz
  //config.autoCsEnable = true;          // CS pin controlled by hardware
  config.autoCsEnable = false;           // CS pin not controlled by hardware
  config.clockMode    = usartClockMode0; // clock idle low, sample on rising/first edge
  config.msbf         = true;            // send MSB first
  config.enable       = usartDisable;    // Make sure to keep USART disabled until it's all set up

  GPIO->USARTROUTE[0].RXROUTE = ((ECG_MISO_PORT << _GPIO_USART_RXROUTE_PORT_SHIFT) | (ECG_MISO_PIN << _GPIO_USART_RXROUTE_PIN_SHIFT));
  GPIO->USARTROUTE[0].TXROUTE = ((ECG_MOSI_PORT << _GPIO_USART_TXROUTE_PORT_SHIFT) | (ECG_MOSI_PIN << _GPIO_USART_TXROUTE_PIN_SHIFT));
  GPIO->USARTROUTE[0].CLKROUTE = ((ECG_CLK_PORT << _GPIO_USART_CLKROUTE_PORT_SHIFT) | (ECG_CLK_PIN << _GPIO_USART_CLKROUTE_PIN_SHIFT));

  // Enable USART interface pins
  GPIO->USARTROUTE[0].ROUTEEN = GPIO_USART_ROUTEEN_RXPEN |    // MISO
	                            GPIO_USART_ROUTEEN_TXPEN |    // MOSI
	                            GPIO_USART_ROUTEEN_CLKPEN;

  USART_InitSync(ECG_USART, &config);
}

void sd_card_spi_init_usart(void)
{
  // Configure GPIO mode for Master
  GPIO_PinModeSet(SD_CARD_CLK_PORT, SD_CARD_CLK_PIN, gpioModePushPull, 0);   // CLK is push pull
  GPIO_PinModeSet(SD_CARD_CS_PORT, SD_CARD_CS_PIN, gpioModePushPull, 1);     // CS is push pull
  GPIO_PinModeSet(SD_CARD_MOSI_PORT, SD_CARD_MOSI_PIN, gpioModePushPull, 1); // TX (MOSI) is push pull
  GPIO_PinModeSet(SD_CARD_MISO_PORT, SD_CARD_MISO_PIN, gpioModeInput, 1);    // RX (MISO) is input

  // Start with default config, then modify as necessary
  USART_InitSync_TypeDef config = USART_INITSYNC_DEFAULT;
  config.master       = true;            // master mode
  config.baudrate     = 20000000;        // CLK freq is 20 MHz
  //config.autoCsEnable = true;          // CS pin controlled by hardware
  config.autoCsEnable = false;           // CS pin not controlled by hardware
  config.clockMode    = usartClockMode0; // clock idle low, sample on rising/first edge
  config.msbf         = true;            // send MSB first
  config.enable       = usartDisable;    // Make sure to keep USART disabled until it's all set up

  GPIO->USARTROUTE[0].RXROUTE = ((SD_CARD_MISO_PORT << _GPIO_USART_RXROUTE_PORT_SHIFT) | (SD_CARD_MISO_PIN << _GPIO_USART_RXROUTE_PIN_SHIFT));
  GPIO->USARTROUTE[0].TXROUTE = ((SD_CARD_MOSI_PORT << _GPIO_USART_TXROUTE_PORT_SHIFT) | (SD_CARD_MOSI_PIN << _GPIO_USART_TXROUTE_PIN_SHIFT));
  GPIO->USARTROUTE[0].CLKROUTE = ((SD_CARD_CLK_PORT << _GPIO_USART_CLKROUTE_PORT_SHIFT) | (SD_CARD_CLK_PIN << _GPIO_USART_CLKROUTE_PIN_SHIFT));

  // Enable USART interface pins
  GPIO->USARTROUTE[0].ROUTEEN = GPIO_USART_ROUTEEN_RXPEN |    // MISO
	                            GPIO_USART_ROUTEEN_TXPEN |    // MOSI
	                            GPIO_USART_ROUTEEN_CLKPEN;

  USART_InitSync(SD_CARD_USART, &config);
}


void spi_initial_wake_init()
{
  usarts_initialized = true;

  CMU_ClockEnable(ecg.cmu_clk, true);
  ecg_spi_init_usart();

  LDMA_Init_t ldmaInit = LDMA_INIT_DEFAULT;
  LDMA_Init(&ldmaInit); // Initializing default LDMA settings
}

// LDMA interrupt handler for ESP32 transfers
void LDMA_IRQHandler()
{
  uint32_t flags = LDMA_IntGet();

  if(flags & ecg.rx_dma->chnl_mask)
	LDMA_IntClear(ecg.rx_dma->chnl_mask);

  if(flags & ecg.tx_dma->chnl_mask)
	LDMA_IntClear(ecg.tx_dma->chnl_mask);

  if(flags & sd_card.rx_dma->chnl_mask)
	LDMA_IntClear(sd_card.rx_dma->chnl_mask);

  if(flags & sd_card.tx_dma->chnl_mask)
	LDMA_IntClear(sd_card.tx_dma->chnl_mask);
}


static void start_dma_transfer(spi_hdle *spi, spi_xfer_cb cb)
{
  spi->xfer_compl = false;
  spi->xfer_compl_cb = cb;

  if(!LDMA_TransferDone(spi->rx_dma->channel))
  {
	LDMA_StopTransfer(spi->rx_dma->channel);
  }
  if(!LDMA_TransferDone(spi->tx_dma->channel))
  {
	LDMA_StopTransfer(spi->tx_dma->channel);
  }

  spi->usart->CMD = USART_CMD_CLEARRX | USART_CMD_CLEARTX;

  LDMA_StartTransfer(spi->rx_dma->channel, &spi->rx_dma->cfg, &spi->rx_dma->descs[0]);
  LDMA_StartTransfer(spi->tx_dma->channel, &spi->tx_dma->cfg, &spi->tx_dma->descs[0]);
}

static void init_select_spi(spi_hdle *spi)
{
  CMU_ClockEnable(cmuClock_PCLK, true);
  CMU_ClockEnable(cmuClock_GPIO, true);
  CMU_ClockEnable(cmuClock_LDMA, true);

  if(!usarts_initialized)
	spi_initial_wake_init();

  CMU_ClockEnable(spi->cmu_clk, true);
  USART_Enable(spi->usart, usartEnable);
}

static void deinit_select_spi(spi_hdle *spi)
{
  USART_Enable(spi->usart, usartDisable);
  CMU_ClockEnable(spi->cmu_clk, false);
  // LEAVE THE GPIO CLOCK RUNNING FOR OTHER PERIPHERALS
}

void ecg_spi_init(void)
{
  init_select_spi(&ecg);
}

void ecg_spi_deinit(void)
{
  deinit_select_spi(&ecg);
}

void sd_card_spi_init(void)
{
  init_select_spi(&sd_card);
}

void sd_card_spi_deinit(void)
{
  deinit_select_spi(&sd_card);
}

void ecg_spi_start_sync_tx(uint8_t *tx_buf, uint8_t *rx_buf, uint32_t len_bytes, spi_xfer_cb cb)
{
  GPIO_PinOutClear(ECG_CS_PORT, ECG_CS_PIN);
  ecg.tx_dma->descs[0] = (LDMA_Descriptor_t)LDMA_DESCRIPTOR_SINGLE_M2P_BYTE(tx_buf, &(ecg.usart->TXDATA), len_bytes);
  ecg.tx_dma->descs[0].xfer.doneIfs = 0;
  ecg.rx_dma->descs[0] = (LDMA_Descriptor_t)LDMA_DESCRIPTOR_SINGLE_P2M_BYTE(&(ecg.usart->RXDATA), rx_buf, len_bytes);
  ecg.rx_dma->descs[0].xfer.doneIfs = 0;
  start_dma_transfer(&ecg, cb);

  while(!LDMA_TransferDone(ecg.tx_dma->channel));
  while(!LDMA_TransferDone(ecg.rx_dma->channel));
  ecg.xfer_compl = true;
  GPIO_PinOutSet(ECG_CS_PORT, ECG_CS_PIN);
  ecg.xfer_compl_cb();
}

void sd_card_spi_start_sync_tx(uint8_t *tx_buf, uint8_t *rx_buf, uint32_t len_bytes, spi_xfer_cb cb)
{
  GPIO_PinOutClear(SD_CARD_CS_PORT, SD_CARD_CS_PIN);
  sd_card.tx_dma->descs[0] = (LDMA_Descriptor_t)LDMA_DESCRIPTOR_SINGLE_M2P_BYTE(tx_buf, &(sd_card.usart->TXDATA), len_bytes);
  sd_card.tx_dma->descs[0].xfer.doneIfs = 0;
  sd_card.rx_dma->descs[0] = (LDMA_Descriptor_t)LDMA_DESCRIPTOR_SINGLE_P2M_BYTE(&(sd_card.usart->RXDATA), rx_buf, len_bytes);
  sd_card.rx_dma->descs[0].xfer.doneIfs = 0;
  start_dma_transfer(&sd_card, cb);

  while(!LDMA_TransferDone(sd_card.tx_dma->channel));
  while(!LDMA_TransferDone(sd_card.rx_dma->channel));
  sd_card.xfer_compl = true;
  GPIO_PinOutSet(SD_CARD_CS_PORT, SD_CARD_CS_PIN);
  sd_card.xfer_compl_cb();
}


