/*
 * fatfs_file.h
 *
 *  Created on: Aug 25, 2020
 *      Author: Harsha Ganegoda
 */

#ifndef SRC_FATFS_FILE_H_
#define SRC_FATFS_FILE_H_

void init_file_buffers(void);
void init_micro_sd(void);
void file_mount(void);
void file_open_for_write();
void file_lseek(uint32_t start_location);
void file_write(uint8_t buffer_to_write[], uint32_t num_bytes);
void file_close(void);


void file_open_for_write_with_file_name(char file_name);

#endif /* SRC_FATFS_FILE_H_ */
