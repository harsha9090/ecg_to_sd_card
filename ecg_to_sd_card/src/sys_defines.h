// Copyright 2018 Firia, Inc.
/* System level definitions for embedded-core */

#ifndef __sys_defines_h
#define __sys_defines_h

// Allow for interrupt-safe operation
#ifndef  FIRIA_CRITICAL_SECTION
#define FIRIA_irqState  void *
#define FIRIA_EnterCritical()  ((void *)0)
#define FIRIA_ExitCritical(irqState)  ((void *)0)
#else
// Import a target-specific critical section if it exists
#include "events_cs.h"
#endif

#endif  // __sys_defines_h
